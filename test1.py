"""
test1
~~~~~~~~~~~~
:copyright: (c) 2021 by David Lu.
:license: MIT, see LICENSE for more details.
"""
from typing import Union
import numpy as np


class Individual:
    """Object storing a search point and its fitness value."""

    def __init__(self, x: np.ndarray, f: float) -> None:
        self.x = x
        self.f = f

    """Instantiation of self.x and self.f"""

    def __mul__(self, other: Union[int, float, np.ndarray, "Individual"]) -> "Individual":
        result = Individual(x=self.x, f=self.f)
        if isinstance(other, Individual):
            assert other.x.shape == result.x.shape
            result.x = result.x * other.x
        elif isinstance(other, np.ndarray):
            assert other.shape == result.x.shape
            result.x = result.x * other
        else:
            result.x = result.x * other
        return result

    """Multiply the value of self.x with the value for other."""

    def __lt__(self, other: "Individual") -> bool:
        return other.f > self.f

    """Return comparision of self.f"""

    def __repr__(self) -> str:
        return str(self.x) + ':' + str(self.f)

    """Representation of Individual object"""


if __name__ == '__main__':
    import random
    i1 = Individual(x=np.random.uniform(0, 1, 10), f=random.random())
    i2 = Individual(x=np.random.uniform(0, 1, 10), f=random.random())
    print(i1)
    i = i1 * i2
    print(i)
    print(i1, i2)
    i = i1 < i2
    print(i)
