import unittest
import numpy as np
import random
from test2 import dataset


class TestDemo(unittest.TestCase):
    """Test mathfuc.py"""

    @classmethod
    def setUpClass(cls):
        print ("this setupclass() method only called once.\n")

    @classmethod
    def tearDownClass(cls):
        print ("this teardownclass() method only called once too.\n")

    def setUp(self):
        print ("do something before test : prepare environment.\n")
        self.data = dataset()

    def tearDown(self):
        print ("do something after test : clean up.\n")

    def test_divide_data(self):
        self.data.divide_dataset(2)
        self.assertEqual(2, len(self.data.divided_data))

    def test_fit(self):
        """Test method fit dataset"""
        self.data.fit('Tasmania,Australia')
        self.assertEqual(1, len(self.data.fit_functions.keys()))


    def test_fix(self):
        """Test method fix dataset"""
        serie = np.random.uniform(0, 1, 5)
        serie = self.data.fix(serie)
        for i in range(len(serie) - 1):
            self.assertLessEqual(serie[i], serie[i + 1])



if __name__ == '__main__':
    unittest.main(verbosity=2)