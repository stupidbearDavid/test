import unittest
import numpy as np
import random
from test1 import Individual


class TestDemo(unittest.TestCase):
    """Test mathfuc.py"""

    @classmethod
    def setUpClass(cls):
        print ("this setupclass() method only called once.\n")

    @classmethod
    def tearDownClass(cls):
        print ("this teardownclass() method only called once too.\n")

    def setUp(self):
        print ("do something before test : prepare environment.\n")
        self.i1 = Individual(x=np.ones(10) * 3, f=0.5)
        self.i2 = Individual(x=np.ones(10) * 2, f=0.2)
        self.i3 = Individual(x=np.ones(10) * 6, f=0.1)

    def tearDown(self):
        print ("do something after test : clean up.\n")

    def test_multiply(self):
        """Test method mul(a, b)"""
        np.testing.assert_array_equal(self.i3.x, (self.i1 * self.i2).x)
        np.testing.assert_array_equal(self.i3.x, (self.i1 * self.i2.x).x)
        np.testing.assert_array_equal(self.i3.x, (self.i1 * 2).x)
        np.testing.assert_array_equal(self.i3.x, (self.i2 * 3.0).x)

    def test_lt(self):
        """Test method lt(a, b)"""
        self.assertLess(self.i3, self.i1)
        self.assertLess(self.i3, self.i2)
        self.assertLess(self.i2, self.i1)

    def test_pre(self):
        """Test method pre(a)"""
        print(self.i1, self.i2, self.i3)


if __name__ == '__main__':
    unittest.main(verbosity=2)