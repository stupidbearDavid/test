"""
test2
~~~~~~~~~~~~

This module implements the time serie forcast.

:copyright: (c) 2021 by David Lu.
:license: MIT, see LICENSE for more details.
"""


import numpy as np
import matplotlib.pyplot as plt
import csv
from statsmodels.tsa.api import Holt

class location(object):

    def __init__(self, name: str, gps: tuple):
        self.name = name
        self.gps = np.array(gps).astype(float)
        self.confirmed_serie = None
        self.death_serie = None
        self.recovered_serie = None
        self.normalize_gps()

    ''' normalize gps data '''
    def normalize_gps(self):
        self.gps = (self.gps + 180) / 360

    ''' pop train dataset from the earlier data'''
    def train_data(self):
        data_length = int(len(self.recovered_serie) * 0.8)
        return {'c_serie': self.confirmed_serie[:data_length],
                'd_serie': self.death_serie[:data_length],
                'r_serie': self.recovered_serie[:data_length]}


class dataset(object):

    '''initialize dataset'''
    def __init__(self):
        self.covid_data = {}
        self.divided_data = []
        self.fit_functions = {}
        with open('./time_series_covid_19_confirmed.csv', 'r', encoding='utf8')as fp:
            c_list = [i for i in csv.reader(fp)]
        with open('./time_series_covid_19_deaths.csv', 'r', encoding='utf8')as fp:
            d_list = [i for i in csv.reader(fp)]
        with open('./time_series_covid_19_recovered.csv', 'r', encoding='utf8')as fp:
            r_list = [i for i in csv.reader(fp)]
        for i in range(1, len(c_list) - 1):
            if c_list[i][2] == '' or c_list[i][3] == '':
                continue
            data = location(c_list[i][0] + ',' + c_list[i][1], (c_list[i][2], c_list[i][3]))
            data.confirmed_serie = np.array(c_list[i][4:]).astype(float)
            self.covid_data[data.name] = data
        for i in range(1, len(d_list) - 1):
            name = d_list[i][0] + ',' + d_list[i][1]
            if name not in self.covid_data.keys():
                continue
            self.covid_data[name].death_serie = np.array(d_list[i][4:]).astype(float)
        for i in range(1, len(r_list) - 1):
            name = r_list[i][0] + ',' + r_list[i][1]
            if name not in self.covid_data.keys():
                continue
            self.covid_data[name].recovered_serie = np.array(r_list[i][4:]).astype(float)

        for name in self.covid_data.keys():
            if self.covid_data[name].death_serie is None or self.covid_data[name].confirmed_serie is None:
                del self.covid_data[name]

    '''plot original data tendency'''
    def plot_tendency(self, name):
        if not name in self.covid_data.keys():
            return
        x = range(len(self.covid_data[name].confirmed_serie))
        plt.plot(x, self.covid_data[name].confirmed_serie, 'b')
        plt.plot(x, self.covid_data[name].death_serie, 'r')
        plt.plot(x, self.covid_data[name].recovered_serie, 'g')
        plt.show()

    '''divide dataset for cross validate'''
    def divide_dataset(self, num):
        for i in range(num):
            self.divided_data.append([])
        i = 0
        for key in self.covid_data.keys():
            self.divided_data[i % num].append(self.covid_data[key])
            i += 1

    '''fit training dataset'''
    def fit(self, name):
        train_data = self.covid_data[name].train_data()
        data = train_data['c_serie'] - train_data['r_serie'] - train_data['d_serie']
        fit = Holt(np.asarray(data)).fit(smoothing_level=0.3, smoothing_slope=0.05)
        self.fit_functions[name] = fit

    '''forcast the recovered_serie'''
    def forcast(self, name):
        Holt_linear = self.fit_functions[name].forecast(len(self.covid_data[name].confirmed_serie))
        data = self.covid_data[name].confirmed_serie - Holt_linear - self.covid_data[name].death_serie
        data = self.fix(data)
        plt.figure(figsize=(16, 8))
        plt.plot(self.covid_data[name].recovered_serie, label='real')
        plt.plot(data, label='fit')
        plt.legend(loc='best')
        plt.show()

    '''fix the increasing serie tendency'''
    def fix(self, data):
        for i in range(1, len(data)):
            if data[i] < data[i - 1]:
                data[i] = data[i - 1]
        return data




if __name__ == '__main__':
    data = dataset()
    print(len(data.covid_data))
    data.divide_dataset(2)
    print(len(data.divided_data), len(data.divided_data[0]), len(data.divided_data[1]))
    data.fit('Tasmania,Australia')
    data.forcast('Tasmania,Australia')
